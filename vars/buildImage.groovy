#!/user/bin/env groovy
def call(String imageName){
    echo "build the app"
    withCredentials([usernamePassword(credentialsId: 'docker-hub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t $imageName ."//用本地dockerfile去build cschen588/java-maven-app:mavenapp-3.0
        sh 'echo $PASS | docker login -u $USER --password-stdin' //dont need host ip because of dockerhub
        sh "docker push $imageName"//注意要用“” groovy 语法
    }
    }